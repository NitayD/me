<?php

	$learner_name = 'Нитай';
	$age = '15';
	$email = 'nitayd2001@gmail.com';
	$city = 'Алматы';
	$infoForMe = 'PHP Junior Developer';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Информация о курсанте Нитай</title>
	<meta charset="utf-8">
	<style type="text/css" rel="stylesheet">
		body{
			background-color: #BFFFB9;
		}
		h1{
			color: #009616;
			padding-left: 25px;
			text-shadow: 3px 3px 15px black;
			text-decoration: blink;
		}
		table{
			text-align: center;
			color: #00874E;
			font-size: 20px;
			padding-left: 50px;
		}
		tr{
			height: 30px;
			border: 1px solid black;
			background-color: #FFFFAF;
		}
		td{
			width: 200px;
			border: 1px solid black;
			background-color: #FFFFAF;
		}
		a{
			color: #00874E;
		}
	</style>
</head>
<body>
	<h1>Страничка пользователя Нитай</h1>
	<table>
		<tr>
			<td>Имя</td>
			<td><?php echo $learner_name; ?></td>
		</tr>
		<tr>
			<td>Возраст</td>
			<td><?php echo $age; ?></td>
		</tr>
		<tr>
			<td>Адрес электронной почты</td>
			<td><a href="mailto:nitayd2001@gmail.com"><?php echo $email; ?></a></td>
		</tr>
		<tr>
			<td>Город</td>
			<td><?php echo $city; ?></td>
		</tr>
		<tr>
			<td>О себе</td>
			<td><?php echo $infoForMe?></td>
		</tr>
	</table>
</body>
</html>