<?php
$text = 'Все числа, попавшие в числовой ряд, являются "Числами Фибоначчи"';
function numberRow() {
	$x = rand(0, 100);
	$var1 = 1;
	$var2 = 1;
	$var3 = NULL;
	echo 'Вам выпало число ' . $x . '. ';
	while (true)
	{
		if ($var1 > $x) {
			echo 'Задуманное число НЕ входит в числовой ряд';
			break;
		}

		else {
			if ($var1 == $x) {
			echo 'Задуманное число входит в числовой ряд';
			break;
			}
			else {
			$var3 = $var1;
			$var1 += $var2;
			$var2 = $var3;
			}
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<style type="text/css">
		html{
			height: 100%;
			font-family: Arial, Helvetica, sans-serif;
		}
		body{
			background: linear-gradient(135deg, rgba(123,215,252,1) 0%,rgba(228,62,249,1) 99%);
		}
		div{
			text-align: center;
			color: #ffd700;
			border-radius: 20px;
			border: 1px solid black;
			background: linear-gradient(to bottom, #7d73ef 0%,#db36a4 99%);
			height: 100px;
			width: 480px;
			margin: auto;
			text-shadow: 0px 0px 10px black;
		}
		p{
			text-align: center;
			border-top: 1px solid gray;
			color: #49423D;
			font-size: 14px;
		}
	</style>
</head>
<body>
	<div>
	<h2><strong><?= numberRow(); ?></strong></h2>
	</div>
	<article>
		<p><strong><?= $text; ?></strong></p>
	</article>
</body>
</html>