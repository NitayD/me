<?php
$animals = array(
	'Africa' => array("Addra Gazelle", "Aardwolf", "Aardvark"),
	'North America' => array("American Alligator", "Caribou", "Arctic Wolf"),
	'South America' => array("Anaconda", "Andean Condor", "Llama"),
	'Asia' => array("Slow Loris", "Elephant", "Giant Panda"),
	'Europa' => array("Wild Boar", "Saiga", "Raccoon Dog"),
	'Australia' => array("Bandicoot", "Cane Toad", "Crocodile")
);
$animalsName = [];
$keysName = [];
//Пустые массивы на будущее
$firstWord = [];
$lastWord = [];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home Work 4</title>
	<meta charset="utf-8">
	<style type="text/css" rel="stylesheet">
		html{
			height: 100%;
			font-family: Arial, Helvetica, sans-serif;
		}
		body{
			background: linear-gradient(135deg, rgba(123,215,252,1) 0%,rgba(228,62,249,1) 99%);
		}
		h1{
			padding-left: 150px;
		}
		#text{
			padding-left: 150px;
		}
		h2{
			text-align: center;
		}
		p{
			text-align: center;
		}
		div{
			border: 1px solid black;
			max-width: 400px;
		}
		#secondPole{
			width: 400px;
			padding-left: 150px;
			padding-bottom: 200px;
		}
		strong{
			margin-left: 10px;
			margin-bottom: 10px;
			text-align: center;
		}
		h3{
			text-align: center;
		}
	</style>
</head>
<body>
	<table>
	<tr>
	<td>
	<?php
	//Этот цикл нужен для выведения всех животных со всех континентов
	foreach ($animals as $key => $value) {
		echo "<h1>Континент $key</h1><h4 id=\"text\">Животные: ";
		foreach ($value as $animal) {
			$animal .= ", ";
			echo "$animal";
		}
		echo " </h4><hr>";
	}
	?>
	</td>
	<td id="secondPole">
	<div>
		<?php
		//Этим циклом мы перетащим слова с 2-мя словами в 2 пустых массива. В предачу к 1-вым словам привяжем континенты
		foreach ($animals as $key => $value) {
			foreach ($value as $animal) {
			//Если слово состоит из 2-вых слов, то запишем его куда надо
				if (count(explode(" ", $animal)) == 2) {
					$animalNameFandS = explode(" ", $animal);
					$firstWord[] = "$key-$animalNameFandS[0]";
					$lastWord[] = $animalNameFandS[1];
				}
			}
		}
		//Мешаем массив со 2-рыми словами
		shuffle($lastWord);
		?>
		<p><h3>Фантастические животные: </h3></p><hr>
		<?php
		//Циклом for вытаскиваем из каждого массива слова и склеиваем их
		$lastContinent = NULL;
		for ($i=0; $i < count($firstWord); $i++) { 
			$continentAndAnimal = explode("-", $firstWord[$i]);
			if ($lastContinent != $continentAndAnimal[0]) {
				echo "<h2>$continentAndAnimal[0]</h2>";
			}
			echo "<strong>$continentAndAnimal[1] $lastWord[$i]</strong><br>";
			//Обязательная переменная, так как в неё нужно записывать последний континент, который был записан
			$lastContinent = $continentAndAnimal[0];
		}
		?>
	</div>
	</td>
	</tr>
	</table>
</body>
</html>