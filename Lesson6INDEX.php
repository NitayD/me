<?php
session_start();
//Если пользователь уже регистрировался, то перекинуть его на Админку
if (isset($_SESSION['admin'])) {
	header('Refresh: 1; admin.php', true, 303);
	exit;
}
//Сканирует папку "Юзеры"
$logins = NULL;
if (scandir('users') !== false) {
	$logins = scandir('users');
	unset($logins[0]);
	unset($logins[1]);
	sort($logins);
}
//Если Юзер нажал на кнопку, то записать введённые им данные в переменную и приписать .json (В дальнейшем будем сравнивать их с файлами в папке users)
if ($_POST['submit']) {
	$userLogin = $_POST['user'];
	$userLogin = $userLogin . '.json';
	//Если в папке users (массиве $logins) есть введённый пользователем логин, то раздекодить json с информацией об этом Юзере
	//Своего рода проверка на логин
	if (in_array($userLogin, $logins)) {
		$fileLogin = file_get_contents(__DIR__ . "/users/" . $userLogin);
		$dataLogin = json_decode($fileLogin, TRUE);
		//Если пароль, введённый Юзером совпадает с тем, что находится в файле, то создать факт авторизации, для полного доступа к сайту
		//Так же отправить пользователя на Админку
		if ($_POST['pass'] == $dataLogin[0]['pass']) {//[0], потому, что при декодировании появляется двумерный массив
			$_SESSION['admin'] = $userLogin;
			$_SESSION['name'] = $dataLogin[0]['name'];
			header('Refresh: 3; admin.php', true, 303);
			echo "<h1 align=\"center\">Успешная авторизация!</h1>";
		}	else	{
			echo "<h1 align=\"center\">Неверный пароль!</h1>";
		}
	}	else	{
		echo "<h1 align=\"center\">Неверный логин!</h1>";
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Авторизация</title>
	<meta charset="utf-8">
	<style type="text/css" rel="stylesheep">
		html{
			height: 100%;
		}
		body{
			background: linear-gradient(to bottom, #7d73ef 0%,#db36a4 99%);
		}
		div{
			padding: auto;
			color: #ffd700;
			width: 300px;
			height: 250px;
			border-radius: 50px;
			border: 1px solid black;
			background: linear-gradient(to bottom, #7d73ef 0%,#db36a4 99%);
			text-shadow: 0px 0px 10px black;
			margin: auto;
			margin-top: 200px;
		}
		a{
			color: #ffd700;
		}
		h1{
			color: #ffd700;
			border: 2px solid black;
			width: 350px;
			margin: auto;
			background: linear-gradient(to bottom, #7d73ef 0%,#db36a4 99%);
			border-radius: 10px;
		}
	</style>
</head>

<body>
<div align="center">
	<p><a href="list.php">Список тестов</a> | <a href="admin.php">Добавить/удалить тесты</a></p>
	<hr>
	<h2>Авторизация</h2>
	<br>
		<form method="POST">
		<table>
		<tr>
			<td>
				Login:
			</td>
			<td>
				<input required type="text" name="user">
			</td>
		</tr>
		<tr>
			<td>
				Password: 
			</td>
			<td>
				<input required type="password" name="pass">
			</td>
		</tr>
		</table>
		<input type="submit" name="submit" value="Войти">
	</form>
</div>
</body>
</html>