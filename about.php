<?php
	$learner_name = 'Нитай';
	$age = '15';
	$email = 'nitayd2001@gmail.com';
	$city = 'Алматы';
	$infoForMe = 'PHP Junior Developer';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Информация о курсанте Нитай</title>
	<meta charset="utf-8">
	<style type="text/css" rel="stylesheet">
		html{
			height: 100%;
			font-family: Arial, Helvetica, sans-serif;
		}
		body{
			background: linear-gradient(135deg, rgba(123,215,252,1) 0%,rgba(228,62,249,1) 99%);
		}
		thead{
			color: #ffd700;
			text-shadow: 0px 0px 25px black;
			text-align: center;
			border-radius: 20px;
			border-bottom: 2px solid black;
		}
		table{
			text-align: center;
			color: #ffd700;
			font-size: 20px;
			margin: auto;
			border-radius: 20px;
			border: 1px solid black;
			background: linear-gradient(to bottom, #7d73ef 0%,#db36a4 99%);
		}
		tr{
			height: 40px;

		}
		td{
			width: 250px;
		}
		a{
			color: #ffd700;
		}
		.info:hover{
			transform: scale(1.1);
			transition:1s transform;
		}
	</style>
</head>
<body>
		<table>
			<caption>
				<td colspan="2" class="info"><strong>Страничка пользователя Нитай</strong></td>
			</caption>
			<tr class="info">
				<td class="info">Имя</td>
				<td class="info"><?php echo $learner_name; ?></td>
			</tr>
			<tr class="info">
				<td class="info">Возраст</td>
				<td class="info"><?php echo $age; ?></td>
			</tr>
			<tr class="info">
				<td class="info">Адрес электронной почты</td>
				<td class="info"><a href="mailto:nitayd2001@gmail.com"><?php echo $email; ?></a></td>
			</tr>
			<tr class="info">
				<td class="info">Город</td>
				<td class="info"><?php echo $city; ?></td>
			</tr>
			<tr class="info">
				<td class="info">О себе</td>
				<td class="info"><?php echo $infoForMe?></td>
			</tr>
		</table>
</body>
</html>