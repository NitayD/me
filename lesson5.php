<?php
    $fileJson = __DIR__ . '/data.json';
    $fileData = file_get_contents($fileJson);
    $data = [];
    $data = json_decode($fileData, true);
    ?>
    <!DOCTYPE html>
    <html>
    <head>
    	<title>Home work 5</title>
    	<meta charset="utf-8">
    	<style type="text/css">
    		thead{
    			background-color: #D350FF;
    			font-weight: bold;
    		}
    		table{
    			text-align: center;
    			border: 2px solid black;
    			background-color: #FCBEFF;
    		}
    		tr{
    			width: 200px;
    		}
    		td{
    			width: 200px;
    		}
    		html{
    			height: 100%;
    		}
    		body{
				font-family: Arial, Helvetica, sans-serif;
				background: linear-gradient(135deg, rgba(123,215,252,1) 0%,rgba(228,62,249,1) 99%);
    		}
    	</style>
    </head>
    <body>
    	<table align="center">
    		<thead>
    			<td>
    				Имя
    			</td>
    			<td>
    				Фамилия
    			</td>
    			<td>
    				Адрес
    			</td>
    			<td>
    				Номер сот. телефона
    			</td>
    		</thead>

    		<tbody>
    			<?php
    				foreach ($data as $key => $v) {
    					echo "<tr>";
    					foreach ($v as $value) {
    						echo "<td>$value</td>";
    				}
    					echo "</tr>";
    			}
    			?>
    		</tbody>
    	</table>
    </body>
    </html>