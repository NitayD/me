<?php
session_start();
//Проверям, был ли зарегестрирован пользователь, если нет, перекидываем его на страницу регистрации и генерируем 403 ошибку
if (!isset($_SESSION['admin'])) {
	http_response_code(403);
	echo "<h1 align=\"center\">Авторизируйтесь как пользователь, что бы получить доступ, к этой странице!</h1><hr><h3 align=\"center\">Через пару секунд вы будете перенаправлены на страницу авторизации</h3>";
	header('Refresh: 5; index.php', true, 303);
	exit;
}
//Если пользователь нажал на кнопку "Выход из аккаунта", то удаляем факт его авторизации и перекидываем на страницу авторизации
if (isset($_GET['exitAccount'])) {
	unset($_SESSION['admin']);
	echo "<h1 align=\"center\">Выходим из аккаунта...</h1>";
	header('Refresh: 2; index.php', true, 303);
	exit;
}
//Функция загрузки файлов на сервер
$dir = 'tests/';
function uploader ($num_of_uploads, $file_types_array, $max_file_size, $upload_dir) {
	if (!is_numeric ($max_file_size)) {
	}
	
	if (!isset ($_POST["submitted"])) {
	$form = "<form action='".$_SERVER['PHP_SELF']."' method='post' enctype='multipart/form-data'>Выберете файл:<br /><input type='hidden' name='submitted' value='TRUE' id='".time()."'><input type='hidden' name='MAX_FILE_SIZE' value='" . $max_file_size . "'>";
	for ($x=0; $x < $num_of_uploads; $x++) {
		$form .= "<input type='file' name='file[]'><br><font color='red'>*</font><br>";
	}
	$form .= "<input required type='submit' value='Upload'><br><font color='red'>*</font>Максимальное кол-во символов: 15. Тест исключительно в разрешении: ";
	for ($x=0; $x < count($file_types_array);$x++) {
		if ($x < count($file_types_array)-1){
			$form .= $file_types_array[$x].", ";
		} else {
			$form .= $file_types_array[$x].".";
		}
	}
	echo($form);
    }else{
	foreach ($_FILES["file"]["error"] as $key => $value) {
		if ($_FILES["file"]["name"][$key]!="") {
 			if ($value == UPLOAD_ERR_OK) {
				$origfilename = $_FILES["file"]["name"][$key];
				$filename = explode(".", $_FILES["file"]["name"][$key]);
				$filenameext = $filename[count($filename)-1];
				unset($filename[count($filename)-1]);
				$filename = implode(".", $filename);
				$filename = substr($filename, 0, 15).".".$filenameext;
				$file_ext_allow = FALSE;
				for ($x=0; $x < count($file_types_array); $x++) {
					if ($filenameext == $file_types_array[$x]) {
						$file_ext_allow = TRUE;
					}
				}
				
					if ($file_ext_allow) {
						if ($_FILES["file"]["size"][$key] < $max_file_size) {
							if (move_uploaded_file($_FILES["file"]["tmp_name"][$key], $upload_dir . $filename)) {
								header('Refresh: 3; list.php', true, 303);
								echo("Файл успешно загружен ( $filename ).");
							} else {
								echo($origfilename." Загрузка не завершена успешно<br>");
							}
						} else {
								echo($origfilename." Файл слишком большой<br>");
						}
					} else {
						echo($origfilename." Недопустимое расширение файла<br>");
					}
			} else {
				echo($origfilename." Файл не был загружен<br>");
			}
		}
	}
	}
}
uploader(1, array('json'), 9999999, $dir);
?>
<br>
<hr>
<h2>Пример теста:</h2>
<img src="primer.png">
<h3>Для того, что-бы сделать ответ правильным, достаточно добавить "_" и любое слово, после варианта ответа</h3><hr>
<br>
<form action="list.php">
	<button type="submit">Перейти к списку тестов</button>
</form>
<form method="GET">
<button type="submit" name="exitAccount">Выйти из аккаунта</button>
</form>