<?php
session_start();
//Сканируем папку tests
if (scandir('tests') !== false) {
	$path = scandir('tests');
	unset($path[0]);
	unset($path[1]);
	sort($path);
}
//Эта функция выводит все тесты, которые есть на сервере
function getTestName ($path) {
	$number = 0;
	foreach ($path as $key => $value) {
		$number++;
		$value = substr($value, 0, -5);
		echo "<label><input required name=\"qest\" type=\"radio\" value=\"$value\">$number. $value</input></label><br>";
	}
	//Незаконченный скрипт удаления теста
	//if (isset($_GET['qest'])) {
	//	echo "<input type=\"submit\" name=\"delete\" value=\"$_GET['qest']\"><br>";
	//	$delFile = $_GET['qest'] . '.json';
	//	echo "string";
	//	unlink($delFile);
	//}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Список тестов</title>
</head>
<body>
<form action="test.php" method="GET">
	<?php
	getTestName ($path);
	?>
	<button type="submit">Выбрать тест</button>
</form>
<form action="admin.php">
	<button type="submit">Добавить новый тест</button>
</form>
</body>
</html>