<?php
session_start();
//Если Юзер нажал на "Получить сертификат, то генерируем сертификат
if (isset($_GET['getCertificate'])) {
	header("Content-Type: image/png");
	//Если Юзер авторизован, то ввести его имя
	if (isset($_SESSION['admin'])) {
		$name = $_SESSION['name'];
	}	else	{//Иначего пусть вводит своё собственное
		$name = $_GET['nameUser'];
	}
	getImageCert($name, $allQ, $trueAnswer);
}
//Проверка на наличие ошибок
error_reporting(-1);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//Принимаем имя теста, выбранное в Листе.пхп
$json = NULL;
if (isset($_GET['qest'])) {
	$json = $_GET['qest'];
}
$dataJson = NULL;

//Получаем все тесты и делаем их разрешениям (.json) харакири
if ($json !== NULL)	{
	if (scandir('tests') !== false) {
		$path = scandir('tests');
		unset($path[0]);
		unset($path[1]);
		sort($path);
		$n = 0;
		foreach ($path as $k => $v) {
			$n++;
			$value = substr($v, 0, -5);
		}
		//Ищем нужный нам тест и декодим его
		if (in_array($json, $path)) {
			$fileJson = file_get_contents(__DIR__ . "/tests/" . $json . ".json");
			$dataJson = json_decode($fileJson, TRUE);
		}
	}
}


$del = NULL;
//На случай, если юзер захочет напрямую подключится к тесту, которого нет
//error_reporting(E_ALL & ~E_NOTICE);
//function myHandler($error) {
 //   switch ($error) {
  //      case E_WARNING:
   //         http_response_code(404);
   //         echo "<h1 align=\"center\">ERROR 404</h1><h3 align=\"center\">Такого теста не существует</h3>";
   //         header('Refresh: 3; list.php', true, 303);
   //         exit();
   //     case E_NOTICE:
   //         http_response_code(404);
    //        echo "<h1 align=\"center\">ERROR 404</h1><h3 align=\"center\">Такого теста не существует</h3>";
  //          header('Refresh: 3; list.php', true, 303);
  //          exit();
  //      default;
   //         return false;
  //  }
//}
// регистрирую обработчик.
//set_error_handler('myHandler', E_ALL);
//Вывод всех вопросов и ответов
function getAnswerAndQuestion($dataJson) {
	$answerNumber = 0;
	foreach ($dataJson as $question => $arrayQuestions) {
		echo "<h1>$question</h1><table><tr>";

		$answerNumber++;
		$answerName = 'q' . $answerNumber;

		foreach ($arrayQuestions as $questionName => $answer) {
			$del = explode("_", $questionName);
			echo "<td><label><input required name=\"$answerName\" type=\"radio\" value=\"$del[0]\">$answer</input></label></td>";
		}
		echo "</tr></table>";
	}
}
//Пустые переменные на будущее (они указанны именно здесь, потому что если пихать в функцию, появляется ошибки)
	$answerNumber = 0;
	$answerName = NULL;
	$trueAnswer = 0;
	$falseAnswer = 0;
	$allQ = NULL;
	$trueA = NULL;
//Функция, которая подсчитывает правильные/неправельные ответы и выдаёт результат
	//аргументы (СписокТестов, просто ноль, пустота, ноль для правильных ответов, неправельных, и для всех вопросов)
function getResult($dataJson, $answerNumber, $answerName, $trueAnswer, $falseAnswer, $allQ) {
	$del = [];
	foreach ($dataJson as $question => $arrayQuestions) {
		$answerNumber++;//С каждым новым вопросом +1
		$answerName = 'q' . $answerNumber;
		foreach ($arrayQuestions as $questionName => $answer) {
			$del = explode("_", $questionName);//делим "a_trueOT" на двое
			if (count($del) === 2) { //если вторая половина ответа == trueOT, то...
				if ($_POST[$answerName] == $del[0]) { //если ответ из ПОСТа[с именем вопроса] == первой половине Правильного ответа, то...
					$trueAnswer++; // + к правельным, иначе ...
				}	else {
					$falseAnswer++; // + к неправельным
				}	
			}
		}
	}

	$allQ = count($dataJson);//Нужно, если пользователь ответил на что-то правильно, на что-то неправельно
	if ($trueAnswer == 0) {
		echo "<h1 align=\"center\">Вы ответили на все вопросы не правильно, пройдите тест ещё раз!</h1>";
	}
	elseif ($falseAnswer == 0) {
		echo "<h1 align=\"center\">Вы ответили на все вопросы правильно, поздравляю!</h1>";
	}
	else {
		echo "<br><h2 align=\"center\">Правильных: $trueAnswer/$allQ</h2><h2 align=\"center\">Неправильных: $falseAnswer/$allQ</h2>";
	}
}
$name = NULL;
//Функция, которая генерирует изображение Сертификата с именем пользователя
//В качестве аргумента используется Имя пользователя
function getImageCert($name) {
	$text = "$name прошёл тест от Н.Д.";
	$date = date("d:m:Y");
	$trueFromAll = "";
	$image = imagecreatetruecolor(640, 464);
	$backColor = imagecolorallocate($image, 31, 31, 31);
	$fontColor = imagecolorallocate($image, 177, 0, 0);
	$fontFile = __DIR__ . '/font/font.ttf';
	$imageCertificate = imagecreatefrompng(__DIR__ . '/certificats/originalCert.png');
	imagefill($image, 0, 0, $backColor);
	imagecopy($image, $imageCertificate, 0, 0, 0, 0, 640, 464);
	imagettftext($image, 18, 0, 178, 182, $fontColor, $fontFile, $text);
	imagettftext($image, 16, 0, 92, 388, $fontColor, $fontFile, $date);
	imagettftext($image, 16, 0, 510, 388, $fontColor, $fontFile, $trueFromAll);
	imagepng($image);
	imagedestroy($image);
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Home work 6</title>
	<meta charset="utf-8">
</head>
<body>
<form class="test" method="POST">
	<?php
	if ($json != NULL) {
	getAnswerAndQuestion($dataJson);
	echo "<button type=\"submit\" name=\"endTest\">Закончить тест!</button>";
	}
	?>
</form>
	<?php
	//Если пользователь закончил тест, то показать результаты
	if (isset($_POST['endTest'])) {
		getResult($dataJson, $answerNumber, $answerName, $trueAnswer, $falseAnswer, $allQ);
		echo "$trueAnswer";
		echo "<style>.test{display: none;}</style>";
		echo "<form action=\"list.php\"><button type=\"submit\" name=\"restartTest\">Перейти к списку тестов</button></form>";
		//Если пользователь не авторизован, то предложить ему ввести своё имя
		echo "<form method=\"GET\">";
		if (!isset($_SESSION['admin'])) {
			echo "Введите ваше имя: <input required name=\"nameUser\" type=\"text\"/>";
		}
		echo "<button type=\"submit\" name=\"getCertificate\">Получить сертификат!</button></form>";
	}
	?>
</body>
</html>